package com.employee.input;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.employee.Employee;

/**
 * Reads input and publishes data to its observers
 * TODO: Add extra subscriber to act upon system commands ex: STOP, EXIT
 *  
 * @author yaswanth
 *
 */
public class InputReader {
	private List<Subscribable> subscribers = new ArrayList<>();
	private Scanner sc;
	
	public void addSubscriber (Subscribable subscriber) {
		subscribers.add(subscriber);
	}
	public void start() {
        sc = new Scanner(System.in);
        
        while(sc.hasNext()) {
        	boolean closeStream = processInputAndPublish(sc.nextLine());
        	if(closeStream) {
        		break;
        	}
        }
        
        closeScanner(sc);
        
	}
	
	private void closeScanner(Scanner sc) {
		if(sc != null)
		{
			sc.close();
			
		}
			
	}
	
	/**
	 * Decides where to publish the input
	 * @param input
	 * @return
	 */
	private boolean processInputAndPublish(String input) {
		 InputCommand enum_input;
		
		/**
		 * Hardcoded check to distinguish command input and data input
		 */
		try
		{
			enum_input = InputCommand.valueOf(input);
		}
		catch(Exception exp)
		{
			enum_input = null;
		}
		finally
		{
		}
		
		// Final declaration so that it can be used in lambda function
		final InputCommand calculatedValue = enum_input;
		
		
		if(calculatedValue != null)
		{
			/**
			 * Publish system data 
			 */
			publish((Subscribable sb) -> {
				sb.notifyOperation(calculatedValue);
			});
			
			if(calculatedValue == InputCommand.EXIT)
			{
				sc.close();
				return true;
			}
			
			return false; 
		}
		
		/**
		 * Pubish normal input data
		 */
		publish((Subscribable sb) -> {
			sb.notifyInput(input);
		});
		
		return false;
	}
	
	/**
	 * Function interface with void return type
	 *
	 * @param <T>
	 */
	interface Function<T>
	{
	    void apply(T t);
	}
	
	private void publish(Function<Subscribable> function) {
		for(Subscribable sb : subscribers) {
			function.apply(sb);
		}
	}
}


