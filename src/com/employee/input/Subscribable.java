package com.employee.input;

public interface Subscribable {

	public void notifyInput(String string);
	public void notifyOperation(InputCommand string);
	
}
