package com.employee.factory;

import com.employee.Employee;
import com.employee.EmployeeInputSubscriber;
import com.employee.network.NetworkService;
import com.employee.repository.EmployeeRepository;
import com.employee.service.EmployeeService;
import com.employee.service.FileWriterService;

public class DependencyInjection {
	public static EmployeeInputSubscriber buildInputSubscriber() throws InterruptedException {
		EmployeeRepository repostitory = new EmployeeRepository();
		NetworkService<Employee> networkService = new FileWriterService<>();
		EmployeeService service = new EmployeeService(repostitory, networkService);
		
		EmployeeInputSubscriber subscriber = new EmployeeInputSubscriber(service);
		
		return subscriber;
	}
}
