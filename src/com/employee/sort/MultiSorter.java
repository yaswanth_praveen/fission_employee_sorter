package com.employee.sort;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

/**
 * Utility class which can use multiple comparators and check
 * 
 * @author yaswanth
 *
 * @param <T>
 */
public class MultiSorter<T> implements Comparator<T> {

	private List<Comparator<T>> comparatorList;

	public MultiSorter(List<Comparator<T>> comparatorList) {
		this.comparatorList = comparatorList;
	}

	@Override
	public int compare(T o1, T o2) {
		if (comparatorList == null || comparatorList.size() == 0) {
			return 0;
		}

		Iterator<Comparator<T>> iterator = comparatorList.iterator();

		while (iterator.hasNext()) {
			int compareResult = iterator.next().compare(o1, o2);

			// If compared result it '0', it goes on to check with next
			// comparator
			if (compareResult == 0) {
				continue;
			}

			return compareResult;
		}
		return 0;
	}

}
