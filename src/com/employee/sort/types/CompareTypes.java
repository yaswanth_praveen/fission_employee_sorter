package com.employee.sort.types;

import java.util.Comparator;

import com.employee.Employee;

/**
 * Enum to ensure comparators are singletons. Lambdas are used to avoid creating multiple comparator classes
 * @author yaswanth
 *
 */
public enum CompareTypes {
	FIRST_NAME(Comparator.comparing(Employee::getFirstName)),
	AGE(Comparator.comparing(Employee::getAge)),
	ORGANIZATION(Comparator.comparing(Employee::getOrganization)),
    LAST_NAME(Comparator.comparing(Employee::getLastName)),
    EXPERIENCE(Comparator.comparing(Employee::getExperience)), RATIO(Comparator.comparing((Employee emp) -> {
        return emp.getExperience() / emp.getAge();
    })), ALL_APPENDED(Comparator.comparing((Employee emp) -> {
        return emp.getFirstName() + emp.getLastName() + emp.getExperience() + emp.getAge() + emp.getOrganization();
    }));


	private Comparator<Employee> comparator;

	private CompareTypes(Comparator<Employee> comparator) {
		this.comparator = comparator;
	}

	public Comparator<Employee> getComparator()
	{
		return comparator;
	}
}
