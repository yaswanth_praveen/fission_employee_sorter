package com.employee.sort.types;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import com.employee.Employee;
import com.employee.sort.MultiSorter;

/**
 * This enum is build according to specification
 * @author yaswanth
 *
 */
public enum SpecConfigs {
	SORT_1 (CompareTypes.ORGANIZATION,
			CompareTypes.EXPERIENCE,
			CompareTypes.FIRST_NAME,
            CompareTypes.LAST_NAME), SORT_2(CompareTypes.RATIO, CompareTypes.ORGANIZATION), SORT_3(CompareTypes.ALL_APPENDED);


    MultiSorter<Employee> multiSorter;
	SpecConfigs(CompareTypes... types) {
        List<Comparator<Employee>> compareTypes = new ArrayList<Comparator<Employee>>();
		for(CompareTypes type : types) {
			compareTypes.add(type.getComparator());
		}

        multiSorter = new MultiSorter<>(compareTypes);
	}

    public Comparator<Employee> getComparator() {
        return multiSorter;
	}
}
