package com.employee;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.employee.input.InputCommand;
import com.employee.input.Subscribable;
import com.employee.service.EmployeeService;

public class EmployeeInputSubscriber implements Subscribable {

	private EmployeeService employeeService;

	public EmployeeInputSubscriber(EmployeeService employeeService) {
		this.employeeService = employeeService;
	}

	@Override
	public void notifyInput(String string) {
		// TODO Auto-generated method stub
		Employee e = EmployeeWrapper.build(string);
		if(e == null) {
			return;
		}
		employeeService.putData(e);
	}

	@Override
	public void notifyOperation(InputCommand command) {
		// TODO Auto-generated method stub
		try {
			employeeService.postData(command);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private static class EmployeeWrapper {
		
		public static Employee build(String employeeString) {
			String[] fragments = employeeString.split(",");
			if(fragments.length < 5)
			{
				System.out.println("Data not inserted as it does not contain right data");
				return null;
			}
			Employee employee = new Employee();
			employee.setFirstName(fragments[0].trim());
			employee.setLastName(fragments[1].trim());
			employee.setExperience(Float.valueOf(fragments[2].trim()));
			employee.setAge(Float.valueOf(fragments[3].trim()));
			employee.setOrganization(fragments[4].trim());
			
			return employee;
		}
		
	}

}

