package com.employee;


public class Constants {

    private static final String OUTPUT_FOLDER_PATH = "../../../";
    public static final String SORT_1_FILE_PATH = OUTPUT_FOLDER_PATH + "sort_1.txt";
    public static final String SORT_2_FILE_PATH = OUTPUT_FOLDER_PATH + "sort_2.txt";
    public static final String SORT_3_FILE_PATH = OUTPUT_FOLDER_PATH + "sort_3.txt";
    
    public static final Integer MAX_TIME_OUT_ON_FORCE_SHUTDOWN_IN_SECONDS = 5;
}

