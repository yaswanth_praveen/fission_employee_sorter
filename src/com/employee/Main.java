package com.employee;

import com.employee.factory.DependencyInjection;
import com.employee.input.InputReader;

public class Main {
	
	
	public static void main(String[] args) throws InterruptedException {
		
		EmployeeInputSubscriber subscriber = DependencyInjection.buildInputSubscriber();
		InputReader reader = new InputReader();
		reader.addSubscriber(subscriber);
		System.out.println("Enter your input");
		reader.start();
		
	}
}
