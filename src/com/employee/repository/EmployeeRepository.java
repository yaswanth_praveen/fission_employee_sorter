package com.employee.repository;

import java.util.ArrayList;
import java.util.List;

import com.employee.Employee;

public class EmployeeRepository {
	private ArrayList<Employee> employeeList = new ArrayList<Employee>();
	
	public synchronized Boolean put(Employee employee) {
		return employeeList.add(employee);
	}
	
	public List<Employee> getEmployees() {
		return employeeList;
	}
	
	public void clear()
	{
		employeeList.clear();
	}
}
