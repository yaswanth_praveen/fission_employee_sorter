package com.employee.service;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

import com.employee.network.NetworkService;
import com.employee.network.TaskExecutor;
import com.employee.network.files.FileWriter;
import com.employee.network.files.FileWriterFactory;

public class FileWriterService<T> implements NetworkService<T> {

    private final TaskExecutor<String> executor;

	public FileWriterService() throws InterruptedException {
    	executor = new TaskExecutor<String>();
	}
	
	private class FileWriteTask implements Callable<String> {

		String data;
		FileWriter writer;
		Object mutex;

		FileWriteTask(FileWriter writer, String data, Object mutex) {
			this.writer = writer;
			this.data = data;
			this.mutex = mutex;
		}

		@Override
		public String call() throws Exception {
			synchronized (mutex) {
				Thread.sleep(5000);
				writer.write(data);
				writer.close();
			}
			return null;
		}
	}

	@Override
	public void post(T entity, String path) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void post(List<T> entities, String path) throws IOException {
		// TODO Auto-generated method stub
		FileWriter writer = FileWriterFactory.getFileWriter(path, Boolean.TRUE);
		
		FileLock mutex = new FileLock(path);
		
		// TODO: Have to change to string to appropriate data
		executor.submit(new FileWriteTask(writer, entities.toString(), mutex));
	}
	
	/**
	 * For lock on file to avoid parallel writes
	 * @author ymediset
	 *
	 */
	static private class FileLock {
		private final String filePath;
		private FileLock(String filePath) {
			this.filePath = filePath;
		}
		
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((filePath == null) ? 0 : filePath.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			FileLock other = (FileLock) obj;
			if (filePath == null) {
				if (other.filePath != null)
					return false;
			} else if (!filePath.equals(other.filePath))
				return false;
			return true;
		}
	}

	@Override
	public void forceShutdown() {
		// TODO Auto-generated method stub
		executor.shutdown();
	}
	

}
