package com.employee.service;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import com.employee.Constants;
import com.employee.Employee;
import com.employee.input.InputCommand;
import com.employee.network.NetworkService;
import com.employee.repository.EmployeeRepository;
import com.employee.sort.types.SpecConfigs;

public class EmployeeService {
	
	/**
	 * Repository access
	 */
	private EmployeeRepository repository;
	
	/**
	 * Network service to post employee data.
	 */
	private NetworkService<Employee> networkService;

	public EmployeeService(EmployeeRepository repository, NetworkService<Employee> fileNetwork) {
		this.repository =repository;
		this.networkService = fileNetwork;
	}
	public boolean putData(Employee employee) {
		return repository.put(employee);
	}

	public void postData(InputCommand command) throws IOException {
		if(command == InputCommand.SORT) {
			sortDataAndWrite();
			repository.clear();
		}
		else if (command == InputCommand.EXIT) {
			networkService.forceShutdown();
		}
	}
	
	private void sortDataAndWrite() throws IOException
	{
		List<Employee> employeeList = repository.getEmployees();
		
		 System.out.println(employeeList);
		 
       Collections.sort(employeeList, SpecConfigs.SORT_1.getComparator());
       System.out.println(employeeList);
       networkService.post(employeeList, Constants.SORT_1_FILE_PATH);
       
      /* FileWriter writer = FileWriterFactory.getSortedFile(SpecConfigs.SORT_1);
		writer.write(employeeList.toString());
		writer.close();*/
		
       Collections.sort(employeeList, SpecConfigs.SORT_2.getComparator());
       System.out.println(employeeList);
       networkService.post(employeeList, Constants.SORT_2_FILE_PATH);
       
       Collections.sort(employeeList, SpecConfigs.SORT_3.getComparator());
       System.out.println(employeeList);
       networkService.post(employeeList, Constants.SORT_3_FILE_PATH);
	}
}
