package com.employee.network;

import java.io.IOException;
import java.util.List;

public interface NetworkService<T> {
	
	public void post(List<T> entities, String path) throws IOException;
	
	public void post(T entity, String path);
	
	public void forceShutdown();
}
