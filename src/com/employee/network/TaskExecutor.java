package com.employee.network;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.employee.Constants;

public class TaskExecutor<V> {
	private ExecutorService executorService = new ThreadPoolExecutor(1, 3, 5000, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	
	public TaskExecutor () throws InterruptedException
	{
		executorService.awaitTermination(Constants.MAX_TIME_OUT_ON_FORCE_SHUTDOWN_IN_SECONDS, TimeUnit.SECONDS);
	}
	
	public Future<V> submit(Callable<V> task) {
		
		return executorService.submit(task);
	}
	
	public void shutdown() {
		executorService.shutdown();
	}
}
