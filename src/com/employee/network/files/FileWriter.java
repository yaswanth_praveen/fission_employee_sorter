package com.employee.network.files;

import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.Writer;

public class FileWriter {

	private final File file;
	private static final String OUTPUT_FOLDER = "";
	private Boolean append = Boolean.FALSE;

	BufferedWriter writer;
	private boolean isConnectionOpen = true;

	public FileWriter(String fileName, Boolean append) throws IOException {

		this.append = append;
		file = new File(OUTPUT_FOLDER + fileName);
		if (!file.exists()) {
			file.createNewFile();
		}
		writer = new BufferedWriter(new java.io.FileWriter(file, append));

		
		// TODO Auto-generated constructor stub
	}

	private Writer getWriter() throws IOException {
		if (isConnectionOpen) {
			return writer;
		}

		return writer = new BufferedWriter(new java.io.FileWriter(file, append));
	}
	
	public void write(String s) throws IOException {
		getWriter().write(s);
	}

	public void close() throws IOException {
		writer.flush();
		writer.close();
		isConnectionOpen = false;
	}

}
