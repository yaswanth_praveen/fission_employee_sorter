package com.employee.network.files;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import com.employee.sort.types.SpecConfigs;

public class FileWriterFactory {

	private static final Map<String, FileWriter> fileWriterMap = new HashMap<String, FileWriter>();

	public static FileWriter getFileWriter(String filePath, Boolean append) throws IOException {
		FileWriter writer = fileWriterMap.get(filePath);

		if (writer != null)
			return writer;

		writer = fileWriterMap.get(filePath);

		if (writer != null)
			return writer;

		writer = new FileWriter(filePath, append);
		fileWriterMap.put(filePath, writer);

		return writer;
	}
}
