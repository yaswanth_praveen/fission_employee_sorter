package com.employee.test;

import java.util.Random;

import com.employee.EmployeeInputSubscriber;
import com.employee.factory.DependencyInjection;

public class Test {

	EmployeeInputSubscriber subscriber = null;
	
	public Test () throws InterruptedException {
		this.subscriber = DependencyInjection.buildInputSubscriber();
	}
	

	public void createEmployees ()
	{
		Random rng = new Random();
		for(int i = 0; i<  20; i++) {
			String input = "fname " + rng.nextInt(50) + ", " + "lname" + rng.nextInt(50) + ","+ rng.nextInt(50) + "," +rng.nextInt(50)/1.0 +"," 
					  + "oracle " + rng.nextInt(50) + ","+ rng.nextInt(50)/1.0f;
		
			subscriber.notifyInput(input);
		}	
		
			subscriber.notifyOperation(null);
		
	}

	public static void main(String[] args) throws InterruptedException {
		new Test().createEmployees();
	}

}
